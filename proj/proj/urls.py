from django.contrib import admin
from django.urls import path, include

from rest_framework import routers
from app_api import views

router = routers.DefaultRouter()
router.register(r'currencies', views.CurrencyItemViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),

    path('', include(router.urls)),
]
