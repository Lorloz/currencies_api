# settings/local.py

from .base import *

DEBUG = True

ALLOWED_HOSTS = ['curr.krascatalog.ru', '127.0.0.1', ]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

SECRET_KEY = 'wBnfkrutKYPaiYwK5rpwOJC2xzBMM0viQQnpYnsHgYnmoqizmMdzRDygMPHnL6M'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
