# settings/production.py

from .base import *

import os

SECRET_KEY = os.environ["CURR_SECRET"]

DEBUG = False

ALLOWED_HOSTS = ['curr.krascatalog.ru', '127.0.0.1', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}
