from .serializers import CurrencyItemSerializer
from app_core.models import CurrencyItem

from rest_framework import viewsets
from rest_framework import permissions


class CurrencyItemViewSet(viewsets.ModelViewSet):
    queryset = CurrencyItem.objects.all().order_by('pk')
    serializer_class = CurrencyItemSerializer
    permission_classes = [permissions.IsAuthenticated, ]
