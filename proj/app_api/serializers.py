from app_core.models import CurrencyItem
from rest_framework import serializers


class CurrencyItemSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CurrencyItem
        fields = ['id', 'name', 'rate', 'url', ]
