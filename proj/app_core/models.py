from django.db import models

# Create your models here.


class CurrencyItem(models.Model):
    name = models.CharField(max_length=50)
    rate = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)

    def __str__(self):
        return '{}: {}'.format(self.name, self.rate)
