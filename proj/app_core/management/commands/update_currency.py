from django.core.management.base import BaseCommand

# from datetime import timedelta, datetime

from urllib.request import urlopen

import xml.etree.ElementTree as ET

from decimal import Decimal as D

from app_core.models import (
    CurrencyItem
)

NEEDED_CURRENCIES_IDS = [
    'R01235',
    'R01239',
]


class Command(BaseCommand):
    help = 'Update currencies'

    def handle(self, *args, **options):
        var_url = urlopen('http://www.cbr.ru/scripts/XML_daily.asp')

        tree = ET.parse(var_url)

        root = tree.getroot()

        for currency in root.findall('Valute'):
            if currency.attrib['ID'] in NEEDED_CURRENCIES_IDS:
                currency_name = currency.find('Name').text
                currency_rate = D(currency.find('Value').text.replace(',', '.'))

                # get or update db records start
                currency_item, created = CurrencyItem.objects.update_or_create(name=currency_name)

                if currency_item.rate != currency_rate:
                    currency_item.rate = currency_rate
                    currency_item.save()

                # get or update db records end

                self.stdout.write(self.style.SUCCESS(currency_item))
