from django.contrib import admin

from .models import CurrencyItem

# Register your models here.

admin.site.register(CurrencyItem)
